package com.example.admin.writejsonfile;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.example.admin.writejsonfile.Models.GPS;
import com.example.admin.writejsonfile.Models.OBD;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileWriter;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;

public class MainActivity extends AppCompatActivity {

    private boolean runtime_permissions()
    {
        if( Build.VERSION.SDK_INT >= 23 &&
                ContextCompat.checkSelfPermission(this,Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                ){
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},100 );
            return true;
        }
        return false;
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
    {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == 100) //request Code wird in Funktion runtime_permissions() gesetzt (Zeile 100)
        {
            if(grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED)
            {
                //System.out.println("Permission granted");
            }
            else
            {
                runtime_permissions();
            }
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(!runtime_permissions())
        {

        }
    }

    public void WriteJSON(View view) {
       ArrayList<GPS> liste=new ArrayList<GPS>();
        try
        {
            liste.add(new GPS("12:23",148,123));
            liste.add(new GPS("12:24",15,1));
            liste.add(new GPS("12:25",1,3));

            JSONArray gsonArray=new JSONArray();
            // Here we convert Java Object to JSON
            JSONObject jsonAddGPS = new JSONObject(); // we need another object to store the coordinates:
            // Create a new JSONObject
            for(int j=0;j<= liste.size()-1;j++){
                JSONObject gson = new JSONObject();
                jsonAddGPS.put("latitude", liste.get(j).latitude);
                jsonAddGPS.put("longitude", liste.get(j).longitude);
                // We add the object to the main object
                gsonArray.put(j,jsonAddGPS);

            }

            //Convert the Json object to JsonString
            String jsonString= gsonArray.toString();

            // Create a new FileWriter object
            String fileName=getFilesDir() +"/log2.json";  // storage/emulated/0/data/log.json
            FileWriter fileWriter = new FileWriter( fileName);

            // Writing the jsonObject into log.json
            fileWriter.write(jsonString);
            fileWriter.close();

            System.out.println("JSON Object Successfully written to the file!!"+getFilesDir().toString());

        } catch (Exception e)
        {
            e.printStackTrace();
        }
        Toast.makeText(this,"JSON Object Successfully written to the file",Toast.LENGTH_LONG).show();
    }
}
