package com.example.admin.writejsonfile.Models;

public class OBD {

    public String value;
    public String timestamp;
    public  String name;

     public OBD(String value,String timestamp,String name){
         this.value=value;
         this.timestamp=timestamp;
         this.name=name;
     }
}
