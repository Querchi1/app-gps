package com.example.admin.writejsonfile.Models;

public class GPS {
    public  String time;
    public double latitude;
    public double longitude;
    public GPS(){};
    public GPS(String time,double latitude ,double longitude){
        this.time=time;
        this.latitude=latitude;
        this.longitude=longitude;
    }
}